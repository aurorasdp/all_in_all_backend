<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
//require APPPATH.'/libraries/REST_Controller.php';
require (APPPATH.'/models/notifications_model.php');

class Notifications extends REST_Controller {

	function sendPushNotification($mode, $userId, $serviceProviderName, $serviceName, $date, $time, $serviceProviderPhone, $serviceProviderId,
									 $carType, $hourlyOrOutStationValue, $appointmentId) {
	   	// Push The notification with parameters
	   	require_once('PushBots.class.php');
	   	$pb = new PushBots();
	   	// Application ID
	   	$appID = '574c490d4a9efa740a8b4567';
	   	// Application Secret
	   	$appSecret = '40b3c5dda8d2c31fb9e8e5e7cc7f48ea';
	   	$pb->App($appID, $appSecret);
	   	$notifications_model = new notifications_model();
	   	// Notification Settings
   		switch ($mode) {
   			case "CANCEL_APPOINTMENT":
   				if ($serviceName == "CALL DRIVER") {
		    		$pb->AlertOne('Your appointment cancelled with service provider: '.$serviceProviderName.' for service: '.$serviceName.' on date: '.$date.' and time: '.$time.' with car type: '.$carType.' and hourly or outstation: '.$hourlyOrOutStationValue);
		    	$msg = 'Your appointment cancelled with service provider: '.$serviceProviderName.' for service: '.$serviceName.' on date: '.$date.' and time: '.$time.' with car type: '.$carType.' and hourly or outstation: '.$hourlyOrOutStationValue;
		    		// echo 'Your appointment cancelled with service provider: '.$serviceProviderName.' for service: '.$serviceName.' on date: '.$date.' and time: '.$time.' with car type: '.$carType.' and hourly or outstation: '.$hourlyOrOutStationValue;
   				} else {
		    		$pb->AlertOne('Your appointment cancelled with service provider: '.$serviceProviderName.' for service: '.$serviceName.' on date: '.$date.' and time: '.$time);
		    	$msg = 'Your appointment cancelled with service provider: '.$serviceProviderName.' for service: '.$serviceName.' on date: '.$date.' and time: '.$time;
		    		// echo 'Your appointment cancelled with service provider: '.$serviceProviderName.' for service: '.$serviceName.' on date: '.$date.' and time: '.$time;
		    	}
		        $regId = $notifications_model->getUserRegID($userId);
		       	$customfields= array("mode" => "REJECT_APPOINTMENT", "appointment_id" => $appointmentId, "message" =>$msg);
		        break;
		    case "CONFIRM_APPOINTMENT":
		    	if ($serviceName == "CALL DRIVER") {
		    		$pb->AlertOne('Your appointment confirmed with service provider: '.$serviceProviderName.' for service: '.$serviceName.' on date: '.$date.' and time: '.$time.' and service provider phone is: '.$serviceProviderPhone.' with car type: '.$carType.' and hourly or outstation: '.$hourlyOrOutStationValue);
		    	//  echo 'Your appointment confirmed with service provider: '.$serviceProviderName.' for service: '.$serviceName.' on date: '.$date.' and time: '.$time.' and service provider phone is: '.$serviceProviderPhone.' with car type: '.$carType.' and hourly or outstation: '.$hourlyOrOutStationValue;
		    	} else {
		    		$pb->AlertOne('Your appointment confirmed with service provider: '.$serviceProviderName.' for service: '.$serviceName.' on date: '.$date.' and time: '.$time.' and service provider phone is: '.$serviceProviderPhone);
		    	 // echo 'Your appointment confirmed with service provider: '.$serviceProviderName.' for service: '.$serviceName.' on date: '.$date.' and time: '.$time.' and service provider phone is: '.$serviceProviderPhone;
		    	}
		        $regId = $notifications_model->getUserRegID($userId);
		        $customfields= array("mode" => "CONFIRM_APPOINTMENT", "appointment_id" => $appointmentId);
		        break;
		    case "PENDING_APPOINTMENT":
		    	if ($serviceName == "CALL DRIVER") {
		    		$pb->AlertOne('A pending appointment waiting confirmation for service: '.$serviceName.' on date: '.$date.' and time: '.$time.' with car type: '.$carType.' and hourly or outstation: '.$hourlyOrOutStationValue);
		    		$msg = 'A pending appointment waiting confirmation for service: '.$serviceName.' on date: '.$date.' and time: '.$time.' with car type: '.$carType.' and hourly or outstation: '.$hourlyOrOutStationValue;
		    		//  echo 'A pending appointment waiting confirmation for service: '.$serviceName.' on date: '.$date.' and time: '.$time.' with car type: '.$carType.' and hourly or outstation: '.$hourlyOrOutStationValue;
		    	} else {
		    		$pb->AlertOne('A pending appointment waiting confirmation for service: '.$serviceName.' on date: '.$date.' and time: '.$time);
		    		$msg = 'A pending appointment waiting confirmation for service: '.$serviceName.' on date: '.$date.' and time: '.$time;
		    	//  echo 'A pending appointment waiting confirmation for service: '.$serviceName.' on date: '.$date.' and time: '.$time;
		    	}
				$regId = $notifications_model->getServiceProviderRegID($serviceProviderId);
				$customfields= array("mode" => "PENDING_APPOINTMENT", "appointment_id" => $appointmentId, "message" =>$msg);
		        break;
		    case "REJECT_APPOINTMENT":
		    		$pb->AlertOne('Your appointment is cancelled for service: '.$serviceName.' on date: '.$date.' and time: '.$time.', you can book again on another date or time');
		    	 	$msg = 'Your appointment is cancelled for service: '.$serviceName.' on date: '.$date.' and time: '.$time.', you can book again on another date or time';
		    	 
		    		// echo 'No available service provider for service: '.$serviceName.' on date: '.$date.' and time: '.$time;
		        $regId = $notifications_model->getUserRegID($userId);
		        $customfields= array("mode" => "REJECT_APPOINTMENT", "message" =>$msg);
		        break;
		    default:
		    	
		    
		}
	   	$pb->PlatformOne("1");
	   	$pb->PayloadOne($customfields);
	   	$pb->TokenOne($regId);
	   	$pb->PushOne();
   }
   
   
}