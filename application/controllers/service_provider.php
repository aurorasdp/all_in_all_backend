<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';
require (APPPATH.'/models/service_provider_model.php');
require (APPPATH.'/models/service_data_model.php');

class Service_provider extends REST_Controller
{
	
	function signup_post() {
    	if(!$this->post('name') || !$this->post('country_code') || !$this->post('phone')
			|| !$this->post('email') || !$this->post('password') || !$this->post('address') || !$this->post('service_id')) {
        	$this->response(array(
        			    'message' => 'Missing parameters',
        				'service_provider_id' => null,
        				'success' => '0'), 200);
        }
		if (!$this->post('reg_id') || $this->post('reg_id') == "") {
        		$this->response(array('message' => 'Error missing registration id',
        						  'service_provider_id' => null,
        						  'success' => '0'), 200);
        }
		$service_provider_model = new service_provider_model();
        if($service_provider_model->alreadyExists($this->post('phone'))) {
        	$this->response(array(
        			'message' => 'Service provider with this phone number already exists',
        			'service_provider_id' => null,
        			'success' => '0'), 200);
        } else { 
        	$serviceProviderId = $service_provider_model->insert($this->post('name'), $this->post('country_code'), $this->post('phone'), $this->post('email'), $this->post('password'), $this->post('reg_id'));
	        if ($serviceProviderId == null) {
	        	$this->response(array('message' => 'Error while inserting to Database',	'service_provider_id' => null, 'success' => '0'), 200);
	        } else {
	        	$service_data_model = new service_data_model();
	        	$serviceDataId = $service_data_model->insert($serviceProviderId, $this->post('service_id'), $this->post('address'));
	       		if ($serviceDataId == null) {
	        		$this->response(array('message' => 'Error while inserting to Database',	'service_provider_id' => null, 'success' => '0'), 200);
	       		} else {
		       		if(!$this->post('profile_pic') || !$this->post('image_extension')) {
						$this->response(array('message' => 'Service provider is registered successfully', 'service_provider_id' => $serviceProviderId, 'success' => '1'), 200);
					} else {
						$path = FCPATH ."uploads/service_providers/".$serviceProviderId.".".$this->post('image_extension');
						if ($service_provider_model->insertServiceProviderProfilePic($serviceProviderId, $path)) {
							file_put_contents($path,base64_decode($this->post('profile_pic')));
							$this->response(array('message' => 'Service provider is registered successfully', 'service_provider_id' => $serviceProviderId, 'success' => '1'), 200);
						} else {
							$this->response(array('message' => 'Error while inserting to Database',	'service_provider_id' => null, 'success' => '0'), 200);
						}
					}
	       		}
	        }
        }
    }
    
	function login_post() {
		if(!$this->post('phone') || !$this->post('password')) {
			$this->response(array('message' => 'Missing parameters',
								  'service_provider_id' => null,
								  'success' => '0'), 200);
		}
        $service_provider_model = new service_provider_model();
		$country_code = null;
        $phone = $this->post('phone');
        if (stripos($this->post('phone'), '0091') !== false && stripos($this->post('phone'), '0091') == 0) {
   			$country_code = '0091';
   			$phone = substr($this->post('phone'), 4);
		} else if (stripos($this->post('phone'), '91') !== false &&  stripos($this->post('phone'), '91') == 0) {
			$country_code = '91';
			$phone = substr($this->post('phone'), 2);
		}
        $serviceProviderId = $service_provider_model->validLoginData($country_code, $phone, $this->post('password'));
        if(!$serviceProviderId) {
        	$this->response(array('message' => 'Invalid phone number or password',
								  'service_provider_id' => null,
								  'success' => '0'), 200);
        } else {
        	if (!$this->post('reg_id') || $this->post('reg_id') == "") {
        		$this->response(array('message' => 'Error missing registration id',
								  'service_provider_id' => $serviceProviderId,
								  'success' => '0'), 200);
        	} else {
        		$service_provider_model->updateRegistrationId($serviceProviderId, $this->post('reg_id'));
        		$this->response(array('message' => 'Service provider logged in successfully',
								  'service_provider_id' => $serviceProviderId,
								  'success' => '1'), 200);
        	}
        	
        }
    }
    
    
	function getWalletData_post() {
		$service_provider_model = new service_provider_model();
    	$walletData = $service_provider_model->getWalletData($this->post('service_provider_id'));
    	if ($walletData) {
    		$this->response(array(
    			'message' => 'Wallet data displayed successfully', 
    			'success' => '1',
    			'wallet_data' => $walletData), 200);
    	} else {
   			$this->response(array(
   				'message' => 'Invalid phone number or password', 
   				'success' => '0',
   				'wallet_data' => null), 400);
    	}
    }
    
	function signout_post() {
        $service_provider_model = new service_provider_model();
		$serviceProviderId = $service_provider_model->removeRegistrationId($this->post('service_provider_id'));
	    $this->response(array('message' => 'Service provider signed out successfully',
							      'service_provider_id' => $serviceProviderId,
							      'success' => '1'), 200);
    }
    
	function sendMailByNewPassword_post() {
		$service_provider_model = new service_provider_model();
		$country_code = null;
        $phone = $this->post('phone');
        if (stripos($this->post('phone'), '0091') !== false && stripos($this->post('phone'), '0091') == 0) {
   			$country_code = '0091';
   			$phone = substr($this->post('phone'), 4);
		} else if (stripos($this->post('phone'), '91') !== false &&  stripos($this->post('phone'), '91') == 0) {
			$country_code = '91';
			$phone = substr($this->post('phone'), 2);
		}
		$serviceProviderData = $service_provider_model->getServiceProviderData($phone);
		if ($serviceProviderData) {
			if(filter_var($serviceProviderData['email'], FILTER_VALIDATE_EMAIL)) {
			    //Email is valid
			    $passwordUpdated = $service_provider_model->updateServiceProviderPassword($serviceProviderData['id'], $this->post('encrypted_password'));
				$msg = "Hi ".$serviceProviderData['name'].",\nYour new password is:".$this->post('unencrypted_password')." for your account with phone number:".$this->post('phone');
				$headers = "From: AllinAll <info@allinall.com> \r\n";
				$mailSent = mail($serviceProviderData['email'], "Password Reset", $msg, $headers);
				if (!$mailSent) {
					$this->response(array('message' => 'Mail not sent',
							      'success' => '0'), 200);
				} else {
					$this->response(array('message' => 'Password reset and mail sent successfully',
							      'success' => '1'), 200);
				}
			} else {
				$this->response(array('message' => 'Invalid email',
							      'success' => '0'), 200);
			}
		} else {
			$this->response(array('message' => 'No service provider with this phone',
							      'success' => '0'), 200);
		}
		
    }
    
   
}