<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';
require (APPPATH.'/models/service_model.php');

class Service extends REST_Controller {
	
	function getServices_post() {
		$service_model = new service_model();
    	$services = $service_model->getServices();
    	if(!$services) {
    		$this->response(array(
    				'message' => 'No services available', 
    				'success' => '0',
    				'services' => null), 200);
    	} else {
    		if ($services) {
    			$this->response(array(
    				'message' => 'Services successfully viewed', 
    				'success' => '1',
    				'services' => $services), 200);
    		}
    	}
    }
    
	function getServiceLabels_post() {
		$service_model = new service_model();
    	$services = $service_model->getServiceLabels();
    	if(!$services) {
    		$this->response(array(
    				'message' => 'No services available', 
    				'success' => '0',
    				'services' => null), 200);
    	} else {
    		if ($services) {
    			$this->response(array(
    				'message' => 'Services labels successfully viewed', 
    				'success' => '1',
    				'services' => $services), 200);
    		}
    	}
    }
    
	function getAvailableServiceProvidersList_post() {
		$service_model = new service_model();
    	$serviceProviders = $service_model->getServiceProvidersList($this->post('service_id'));
    	if(!$serviceProviders) {
    		$this->response(array(
    				'message' => 'No service providers available for this service', 
    				'success' => '0',
    				'service_providers' => null), 200);
    	} else {
    		if ($serviceProviders) {
    			$this->response(array(
    				'message' => 'Available service providers for this service successfully viewed', 
    				'success' => '1',
    				'service_providers' => $serviceProviders), 200);
    		}
    	}
    }
    
    
    
   
}
