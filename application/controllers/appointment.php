<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';
require (APPPATH.'/models/appointment_model.php');
require (APPPATH.'/models/service_provider_model.php');
require (APPPATH.'/models/service_model.php');
require (APPPATH.'/controllers/notifications.php');


class Appointment extends REST_Controller {
	
	function getServiceProviderBookings_post() {
    	if ($this->post('new_login') == "1") {
    		$service_provider_model = new service_provider_model();
    		$service_provider_model->incrementApplicationUsageCount($this->post('service_provider_id'));
    	}
    	$this->load->helper('date');
		date_default_timezone_set("Asia/Kolkata");
		$currentDate = date('Y-m-d');
		$currentTime = date('H:i:s');
		$dateBeforeOneHour = date('Y-m-d', strtotime($currentTime . ' -1 hour'));
		$timeBeforeOneHour = date('H:i:s', strtotime($currentTime . ' -1 hour'));
    	$appointment_model = new appointment_model();
    	$appointmentsData = $appointment_model->getConfirmedAppointmentsDataByServiceProviderId($this->post('service_provider_id'), $dateBeforeOneHour, $timeBeforeOneHour);
    	if ($appointmentsData) {
    		$i = 0;
	    	foreach ($appointmentsData as $data) {
	    		if ($appointmentsData[$i]['user_pic'] != null) {
		   			$pic_url = $appointmentsData[$i]['user_pic'];
		   			$appointmentsData[$i]['user_pic'] = "";
					$file = file_get_contents($pic_url);
					$appointmentsData[$i]['user_pic'] = base64_encode($file);
	    		}
				$i = $i + 1;
			}    			
    		$this->response(array(
    			'message' => 'Service provider bookings successfully viewed', 
    			'success' => '1',
    			'bookings' => $appointmentsData), 200);
    	} else {
    		$this->response(array(
    			'message' => 'No service provider bookings exist', 
    			'success' => '1',
    			'bookings' => null), 200);
    	}
    }
  
	function cancelAppointment_post() {
		$appointment_model = new appointment_model();
	    $appointmentCancelled = $appointment_model->cancelAppointmentById($this->post('appointment_id'));
	    if ($appointmentCancelled) {
	    	$appointmentData = $appointment_model->getAppointmentsDataByAppointmentId($this->post('appointment_id'));
	    	$notification = new Notifications();
    		$notification->sendPushNotification("CANCEL_APPOINTMENT", $appointmentData['user_id'], $appointmentData['service_provider_name'], $appointmentData['service'], $appointmentData['appointment_date'], $appointmentData['appointment_time'],
    											  $appointmentData['service_provider_phone'], null, $appointmentData['car_type'], $appointmentData['hourly_or_outstation'], $this->post('appointment_id'));
	    	$this->response(array(
	    		'message' => 'Appointment cancelled successfully', 
	    		'success' => '1'), 200);
	    } else {
	   		$this->response(array(
	   			'message' => 'No Appointment exist', 
	   			'success' => '0'), 200);
	    }
    }
    
	function confirmAppointment_post() {
		$appointment_model = new appointment_model();
		$appointmentData = $appointment_model->getAppointmentsDataByAppointmentId($this->post('appointment_id'));
		$this->load->helper('date');
		date_default_timezone_set("Asia/Kolkata");
		$appointmentTime = $appointmentData['appointment_date']." ".$appointmentData['appointment_time'];
		$dateBeforeOneHour = date('Y-m-d', strtotime($appointmentTime . ' -1 hour'));
		$timeBeforeOneHour = date('H:i:s', strtotime($appointmentTime . ' -1 hour'));
		$dateAfterOneHour = date('Y-m-d', strtotime($appointmentTime . ' +1 hour'));
		$timeAfterOneHour = date('H:i:s', strtotime($appointmentTime . ' +1 hour'));
		$count = $appointment_model->countConfirmedAppointmentsWithinTimes($appointmentData['service_provider_id'], $dateBeforeOneHour, $timeBeforeOneHour, $dateAfterOneHour, $timeAfterOneHour, $this->post('appointment_id'));
		$isCancelled = $appointment_model->isAppointmentCancelled($this->post('appointment_id'));
		if ($isCancelled) {
			$this->response(array('message' => 'Appointment is cancelled due to late reply', 'appointment_data' => null, 'success' => '0'), 200);
		} else {
			if ($count > 0) {
		   		//cancel appointment because service provider busy
		   		$appointmentCancelled = $appointment_model->cancelAppointmentById($this->post('appointment_id'));
		    	if ($appointmentCancelled) {
			    	//book with next service provider
			   		$returnData = $this->saveRejectedAndBookAgain($appointmentData, true);
			   		if ($returnData['correctBookingDates']) {
			   			$this->response(array(
				   			'message' => 'Service provider cannot confirm this appointment because he is busy at that time, appointment is booked with next service provider', 
				   			'success' => '0'), 200, true);
			   			//Add buffer of time then reject and book with next provider if provider did not reply
						$this->checkAndUpdateBookingStatus($returnData['newAppointmentId']);
			   		} else {
			   			$this->response(array('message' => 'Service provider cannot confirm this appointment because he is busy at that time, error while saving appointment with next service provider, only appointment in future are booked successfully please revise your booking date', 'appointment_data' => null, 'success' => '0'), 200);
			   		}
		    	} else {
		    		$this->response(array(
		   			'message' => 'No Appointment exist', 
		   			'success' => '0'), 200);
		    	}
		   	} else {
			   	$appointmentConfirmed = $appointment_model->confirmAppointmentById($this->post('appointment_id'));
			    if ($appointmentConfirmed) {
			    	$notification = new Notifications();
		    		$notification->sendPushNotification("CONFIRM_APPOINTMENT", $appointmentData['user_id'], $appointmentData['service_provider_name'], $appointmentData['service'], $appointmentData['appointment_date'], $appointmentData['appointment_time'],
		    											 $appointmentData['service_provider_phone'], null, $appointmentData['car_type'], $appointmentData['hourly_or_outstation'], $this->post('appointment_id'));
			    	$this->response(array(
			    		'message' => 'Appointment confirmed successfully', 
			    		'success' => '1'), 200);
			    } else {
			   		$this->response(array(
			   			'message' => 'No Appointment exist', 
			   			'success' => '0'), 200);
			    }
		   	}
		}
    }
    
    
    
	function completeAppointment_post() {
		$appointment_model = new appointment_model();
	    $appointmentCompleted = $appointment_model->completeAppointmentById($this->post('appointment_id'));
	    if ($appointmentCompleted) {
	    	$this->response(array(
	    		'message' => 'Appointment completed successfully', 
	    		'success' => '1'), 200);
	    } else {
	   		$this->response(array(
	   			'message' => 'No Appointment exist', 
	   			'success' => '0'), 200);
	    }
    }
    
	function rejectAppointment_post() {
		$this->rejectAppointmentById($this->post('appointment_id'), true);
    }
    
    function rejectAppointmentById($appointmentId, $returnResponse) {
    	$appointment_model = new appointment_model();
    	$isCancelled = $appointment_model->isAppointmentCancelled($appointmentId);
		if ($isCancelled && $returnResponse) {
			$this->response(array(
		    		'message' => 'Appointment cancelled due to late reply', 
		    		'success' => '0'), 200);
		} else {
		    $appointmentCancelled = $appointment_model->cancelAppointmentById($appointmentId);
		    if ($appointmentCancelled) {
		    	$appointmentData = $appointment_model->getAppointmentsDataByAppointmentId($appointmentId);
		    	//book with next service provider
			   	$returnData = $this->saveRejectedAndBookAgain($appointmentData, $returnResponse);
			   	if ($returnData['correctBookingDates']) {
			   		if ($returnResponse) {
			   			$this->response(array(
			    		'message' => 'Appointment rejected successfully, and booked for next service provider', 
			    		'success' => '1'), 200, true);
			   		}		   		
			   		//Add buffer of time then reject and book with next provider if provider did not reply
					$this->checkAndUpdateBookingStatus($returnData['newAppointmentId']);
			   	} else {
			   		if ($returnResponse) {
				   		$this->response(array('message' => 'Error while saving appointment with next service provider, only appointment in future are booked successfully please revise your booking date', 'appointment_data' => null, 'success' => '0'), 200);
			   		}
			   	}
		    } else {
		    	if ($returnResponse) {
		   			$this->response(array(
		   			'message' => 'No Appointment exist', 
		   			'success' => '0'), 200);
		    	}
		    }
		}
    }
    
	function saveRejectedAndBookAgain($appointmentData, $returnResponse) {
		$appointment_model = new appointment_model();
    	$rejectedServiceProviders = $appointmentData['rejected_service_providers'].$appointmentData['service_provider_id'].",";
	    $appointment_model->saveRejectedServiceProvider($appointmentData['appointment_id'], $rejectedServiceProviders);
		date_default_timezone_set("Asia/Kolkata");
		$currentDate = date('Y-m-d');
		$currentTime = date('H:i:s');
		$correctBookingDates = true;
		$newAppointmentId = null;
		if ($appointmentData['book_now'] == "1" || $appointmentData['appointment_date'] > $currentDate || ($appointmentData['appointment_date'] == $currentDate && $appointmentData['appointment_time'] > $currentTime)) {
			$correctBookingDates = true;
		} else {
			$correctBookingDates = false;
		}
		if ($correctBookingDates) {
			$service_model = new service_model();
			$serviceProviders = $service_model -> getServiceProvidersList($appointmentData['service_id']);
			$rejectedServiceProvidersList = explode(",", $appointmentData['rejected_service_providers'].$appointmentData['service_provider_id']);
			$serviceProvidersList = array();
			$j = 0;
			if ($serviceProviders) {
    			$i = 0;
	    		foreach ($serviceProviders as $serviceProvider) {
	    			if ($rejectedServiceProvidersList) {
	    				if (!in_array($serviceProviders[$i]['service_provider_id'], $rejectedServiceProvidersList)) {
	    					array_push($serviceProvidersList, $serviceProviders[$i]);
	    				}
	    			}
	    			$i = $i + 1;
			   	}
				$serviceProviderId = $this->getServiceProviderFromQueue($appointmentData['user_longitude'], $appointmentData['user_latitude'], $serviceProvidersList, $appointmentData['appointment_date'], $appointmentData['appointment_time']);
				if ($serviceProviderId != null) {
					$appointmentId = $appointment_model->insert($appointmentData['user_id'], $serviceProviderId,
										$appointmentData['appointment_date'], $appointmentData['appointment_time'], $appointmentData['service_id'],
										$appointmentData['user_longitude'], $appointmentData['user_latitude'],
										$appointmentData['car_type'], $appointmentData['hourly_or_outstation'], $rejectedServiceProviders, $appointmentData['book_now']);
					if ($appointmentId != null) {
						//insert appointment code
						$this->generateAndSaveAppointmentCode($appointmentId, $serviceProviderId, $appointmentData['service_id']);
						$appointmentBookData = $appointment_model->getInsertedAppointmentsData($appointmentId);
						$notification = new Notifications();
		    			$notification->sendPushNotification("PENDING_APPOINTMENT", 	null, $appointmentBookData['service_provider_name'], $appointmentBookData['service_name'], $appointmentBookData['appointment_date'], $appointmentBookData['appointment_time'], null,
		    												 $appointmentBookData['service_provider_id'], $appointmentBookData['car_type'], $appointmentBookData['hourly_or_outstation'], $appointmentId);
						$newAppointmentId = $appointmentId;
					} else {
						if ($returnResponse) {
					        $this->response(array('message' => 'Error while inserting to Database', 'appointment_data' => null, 'success' => '0'), 200);
						}
					}
				} else {
					$serviceName = $service_model -> getServiceName($appointmentData['service_id']);
					$notification = new Notifications();
		    		$notification->sendPushNotification("REJECT_APPOINTMENT", $appointmentData['user_id'], null, $serviceName, $appointmentData['appointment_date'], $appointmentData['appointment_time'], null,
		    												 null, null, null, null);
		    		if ($returnResponse) {
		    			$this->response(array('message' => 'No available service provider', 'appointment_data' => null, 'success' => '0'), 200);
		    		}
				}
			} else {
				if ($returnResponse) {
					$this->response(array('message' => 'No available service provider', 'appointment_data' => null, 'success' => '0'), 200);
				}
			}
		} 
		$returnData = array();
		$returnData['correctBookingDates'] = $correctBookingDates;
		$returnData['newAppointmentId'] = $newAppointmentId;
		return $returnData;
    }
    
	function reviewAppointment_post() {
		$appointment_model = new appointment_model();
	    $appointmentReviewed = $appointment_model->reviewAppointmentById($this->post('appointment_id'), $this->post('review'));
	    if ($appointmentReviewed) {
	    	$this->response(array(
	    		'message' => 'Appointment reviewed successfully', 
	    		'success' => '1'), 200);
	    } else {
	   		$this->response(array(
	   			'message' => 'No Appointment exist', 
	   			'success' => '0'), 200);
	    }
    }
    
	function bookAppointment_post() {
		$appointment_model = new appointment_model();
		$this->load->helper('date');
		date_default_timezone_set("Asia/Kolkata");
		$currentDate = date('Y-m-d');
		$currentTime = date('H:i:s');
		$correctBookingDates = true;
		$appointmentsNumber = $appointment_model->getPendingAppointmentsNumberForUser($this->post('user_id'), $currentDate, $currentTime);
		if ($appointmentsNumber >= 5) {
			$this->response(array('message' => 'User reached the booking limit, please wait for some bookings to be confirmed to be able to book new appointments.', 'appointment_data' => null, 'success' => '0'), 200);
		} else {
			if ($this->post('book_now') == "1" || $this->post('date') > $currentDate || ($this->post('date') == $currentDate && $this->post('time') > $currentTime)) {
				$correctBookingDates = true;
			} else {
				$correctBookingDates = false;
			}
			if ($correctBookingDates) {
				$service_model = new service_model();
				$serviceProviders = $service_model -> getServiceProvidersList($this->post('service_id'));
				if ($serviceProviders) {
					$serviceProviderId = $this->getServiceProviderFromQueue($this->post('user_longitude'), $this->post('user_latitude'), $serviceProviders, $this->post('date'), $this->post('time'));
					if ($serviceProviderId != null) {
						$appointmentId = $appointment_model->insert($this->post('user_id'), $serviceProviderId,
							 $this->post('date'), $this->post('time'), $this->post('service_id'), $this->post('user_longitude'), $this->post('user_latitude'),
							 $this->post('car_type'), $this->post('hourly_or_outstation'), null, $this->post('book_now'));
						if ($appointmentId != null) {
							//insert appointment code
							$this->generateAndSaveAppointmentCode($appointmentId, $serviceProviderId, $this->post('service_id'));
							$appointmentData = $appointment_model->getInsertedAppointmentsData($appointmentId);
							if ($appointmentData && $appointmentData['profile_pic_url'] != null) {
								$pic_url = $appointmentData['profile_pic_url'];
							   	$appointmentData['profile_pic_url'] = "";
								$file = file_get_contents($pic_url);
								$appointmentData['profile_pic_url'] = base64_encode($file);
						    }
							$serviceProvidersList = $serviceProviders;
							$notification = new Notifications();
		    				$notification->sendPushNotification("PENDING_APPOINTMENT", null, $appointmentData['service_provider_name'], $appointmentData['service_name'], $appointmentData['appointment_date'], $appointmentData['appointment_time'], null,
		    												 $appointmentData['service_provider_id'], $appointmentData['car_type'], $appointmentData['hourly_or_outstation'], $appointmentId);
							$appointmentDataToSend = $this->getDataToSend($appointmentData);
		    				$this->response(array('message' => 'Appointment booked successfully', 'appointment_data' => $appointmentDataToSend, 'success' => '1'), 200, true);
							//Add buffer of time then reject and book with next provider if provider did not reply
							$this->checkAndUpdateBookingStatus($appointmentId);
						} else {
				        	$this->response(array('message' => 'Error while inserting to Database', 'appointment_data' => null, 'success' => '0'), 200);
				        }
					} else {
						$serviceName = $service_model -> getServiceName($this->post('service_id'));
						$notification = new Notifications();
		    			$notification->sendPushNotification("REJECT_APPOINTMENT", $this->post('user_id'), null, $serviceName, $this->post('date'), $this->post('time'), null,
		    												 null, null, null, null);							
						$this->response(array('message' => 'No available service provider', 'appointment_data' => null, 'success' => '0'), 200);
					}
				} else {
					$this->response(array('message' => 'No available service provider', 'appointment_data' => null, 'success' => '0'), 200);
				}
			} else {
				$this->response(array('message' => 'Error while saving appointment, only appointment in future are booked successfully please revise your booking date', 'appointment_data' => null, 'success' => '0'), 200);
			}
		}
    }
      
    function getUserBookings_post() {
		$this->load->helper('date');
		date_default_timezone_set("Asia/Kolkata");
		$currentDate = date('Y-m-d');
		$currentTime = date('H:i:s');
		$dateBeforeOneHour = date('Y-m-d', strtotime($currentTime . ' -1 hour'));
		$timeBeforeOneHour = date('H:i:s', strtotime($currentTime . ' -1 hour'));
		$appointment_model = new appointment_model();
    	$appointmentsData = $appointment_model->getAppointmentsDataByUserId($this->post('user_id'), $dateBeforeOneHour, $timeBeforeOneHour);
    	if ($appointmentsData) {
    		$i = 0;
	    	foreach ($appointmentsData as $data) {
	    		if ($appointmentsData[$i]['service_provider_pic'] != null) {
		    		$pic_url = $appointmentsData[$i]['service_provider_pic'];
		    		$appointmentsData[$i]['service_provider_pic'] = "";
					$file = file_get_contents($pic_url);
					$appointmentsData[$i]['service_provider_pic'] = base64_encode($file);
    		}
				$i = $i + 1;
		} 
    		$this->response(array(
    			'message' => 'User bookings successfully viewed', 
    			'success' => '1',
    			'bookings' => $appointmentsData), 200);
    	} else {
    		$this->response(array(
    			'message' => 'No user bookings exist', 
    			'success' => '1',
   			'bookings' => null), 200);
    	}
     }
	
	function getHistoryForUser_post() {
		$this->load->helper('date');
		date_default_timezone_set("Asia/Kolkata");
		$currentDate = date('Y-m-d');
		$currentTime = date('H:i:s');
		$dateBeforeOneHour = date('Y-m-d', strtotime($currentTime . ' -1 hour'));
		$timeBeforeOneHour = date('H:i:s', strtotime($currentTime . ' -1 hour'));
		$appointment_model = new appointment_model();
    	$historyData = $appointment_model->getHistoryByUserId($this->post('user_id'), $dateBeforeOneHour, $timeBeforeOneHour);
    	if ($historyData) {
    		$i = 0;
	    	foreach ($historyData as $data) {
	    		if ($historyData[$i]['service_provider_pic'] != null) {
		    		$pic_url = $historyData[$i]['service_provider_pic'];
		    		$historyData[$i]['service_provider_pic'] = "";
					$file = file_get_contents($pic_url);
					$historyData[$i]['service_provider_pic'] = base64_encode($file);
	    		}
				$i = $i + 1;
			} 
    		$this->response(array(
    			'message' => 'User history successfully viewed', 
    			'success' => '1',
    			'history' => $historyData), 200);
    	} else {
    		$this->response(array(
    			'message' => 'No user history exist', 
    			'success' => '1',
    			'history' => null), 200);
    	}
    }
    
    function getServiceProviderFromQueue($userLongitude, $userLatitude, $serviceProvidersList, $appointmentDate, $appointmentTime) {
    	//remove busy service providers
    	$availableServiceProvidersList = $this->removeBusyServiceProvidersFromList($appointmentDate, $appointmentTime, $serviceProvidersList);
    	$maxAppUsageCount = null;
    	$minBookingsCount = null;
    	$minDistance = null;
    	$i = 0;
		$serviceProviders = array();
		foreach ($availableServiceProvidersList as $serviceProvider) {
			$distance = $this->vincentyGreatCircleDistance($serviceProvider['latitude'], $serviceProvider['longitude'], $userLatitude, $userLongitude, 6371);
			if ($distance > 30) {
				continue;
			}
			if ($minDistance == null || $distance < $minDistance) {
				$minDistance = $distance;
			}
			if ($maxAppUsageCount == null || $serviceProvider['application_usage_count'] > $maxAppUsageCount) {
				$maxAppUsageCount = $serviceProvider['application_usage_count'];
			}
			if ($minBookingsCount == null || $serviceProvider['bookings_count'] < $minBookingsCount) {
				$minBookingsCount = $serviceProvider['bookings_count'];
			}
			
			$serviceProviders[$i]['service_provider_id'] = $serviceProvider['service_provider_id'];
			$serviceProviders[$i]['distance'] = $distance;
			$serviceProviders[$i]['bookings_count'] = $serviceProvider['bookings_count'];
			$serviceProviders[$i]['application_usage_count'] = $serviceProvider['application_usage_count'];
			$i = $i + 1;
		}
		$minSum = null;
		$selectedServiceProviderId = null;
    	foreach ($serviceProviders as $serviceProvider) {
			$sum = abs($serviceProvider['distance'] - $minDistance) + abs($serviceProvider['bookings_count'] - $minBookingsCount) + abs($serviceProvider['application_usage_count'] - $maxAppUsageCount);
			if ($minSum == null || $minSum > $sum) {
				$minSum = $sum;
				$selectedServiceProviderId = $serviceProvider['service_provider_id'];
			}
		}
		return $selectedServiceProviderId;
    }
    
    function removeBusyServiceProvidersFromList($appointmentDate, $appointmentTime, $serviceProvidersList) {
    	$appointment_model = new appointment_model();
		$this->load->helper('date');
		date_default_timezone_set("Asia/Kolkata");
		$appointmentTime = $appointmentDate." ".$appointmentTime;
		$dateBeforeOneHour = date('Y-m-d', strtotime($appointmentTime . ' -1 hour'));
		$timeBeforeOneHour = date('H:i:s', strtotime($appointmentTime . ' -1 hour'));
		$dateAfterOneHour = date('Y-m-d', strtotime($appointmentTime . ' +1 hour'));
		$timeAfterOneHour = date('H:i:s', strtotime($appointmentTime . ' +1 hour'));
    	$finalServiceProvidersList = array();
		if ($serviceProvidersList) {
	    	foreach ($serviceProvidersList as $serviceProvider) {
	    		$count = $appointment_model->countBusyAppointments($serviceProvider['service_provider_id'], $dateBeforeOneHour, $timeBeforeOneHour, $dateAfterOneHour, $timeAfterOneHour);
	    		if ($count == 0) {
	   				array_push($finalServiceProvidersList, $serviceProvider);
	   			}
	    	}
		}
		return $finalServiceProvidersList;
    }
    
	/**
	 * Calculates the great-circle distance between two points, with
	 * the Vincenty formula.
	 * @param float $latitudeFrom Latitude of start point in [deg decimal]
	 * @param float $longitudeFrom Longitude of start point in [deg decimal]
	 * @param float $latitudeTo Latitude of target point in [deg decimal]
	 * @param float $longitudeTo Longitude of target point in [deg decimal]
	 * @param float $earthRadius Mean earth radius in [m]
	 * @return float Distance between points in [m] (same as earthRadius)
	 */
 	function vincentyGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000) {
	  // convert from degrees to radians
	  $latFrom = deg2rad($latitudeFrom);
	  $lonFrom = deg2rad($longitudeFrom);
	  $latTo = deg2rad($latitudeTo);
	  $lonTo = deg2rad($longitudeTo);
	
	  $lonDelta = $lonTo - $lonFrom;
	  $a = pow(cos($latTo) * sin($lonDelta), 2) + pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
	  $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);
	
	  $angle = atan2(sqrt($a), $b);
	  return $angle * $earthRadius;
	}
	
 	function getPendingAppointments_post() {
		$appointment_model = new appointment_model();
    	$appointmentsData = $appointment_model->getPendingAppointments($this->post('service_provider_id'));
    	$appointmentIds = "";
    	$notificationMessages = "";
    	if ($appointmentsData) {
    		$i = 0;
	    	foreach ($appointmentsData as $data) {
	    		$appointmentIds = $appointmentIds.','.$appointmentsData[$i]['appointment_id'];
	    		if ($appointmentsData[$i]['service_name'] == "CALL DRIVER") {
		    		$pendingNotificationMessage = 'A pending appointment waiting confirmation for service: '.$appointmentsData[$i]['service_name'].' on date: '.$appointmentsData[$i]['appointment_date'].' and time: '.$appointmentsData[$i]['appointment_time'].' with car type: '.$appointmentsData[$i]['car_type'].' and hourly or outstation: '.$appointmentsData[$i]['hourly_or_outstation'];
		    	} else {
		    		$pendingNotificationMessage = 'A pending appointment waiting confirmation for service: '.$appointmentsData[$i]['service_name'].' on date: '.$appointmentsData[$i]['appointment_date'].' and time: '.$appointmentsData[$i]['appointment_time'];
		    	}
	    		$notificationMessages = $notificationMessages.','.$pendingNotificationMessage;
				$i = $i + 1;
			} 
    		$this->response(array(
    			'message' => 'Pending appointments successfully viewed', 
    			'success' => '1',
    			'appointment_ids' => trim($appointmentIds,','),
    			'notification_messages' => trim($notificationMessages,',')), 200);
    	} else {
    		$this->response(array(
    			'message' => 'No pending appointments exist', 
    			'success' => '0',
   				'appointment_ids' => null,
    			'notification_messages' => null), 200);
    	}
     }
	
	function getDataToSend($appointmentData) {
		$appointmentDataToSend = array();
		if ($appointmentData) {
    		$appointmentDataToSend['appointment_id'] = $appointmentData['appointment_id'];
			$appointmentDataToSend['appointment_code'] = $appointmentData['appointment_code'];
			$appointmentDataToSend['service_provider_name'] = $appointmentData['service_provider_name'];
			$appointmentDataToSend['appointment_date'] = $appointmentData['appointment_date'];
			$appointmentDataToSend['appointment_time'] = $appointmentData['appointment_time'];
			$appointmentDataToSend['status'] = $appointmentData['status'];
			$appointmentDataToSend['address'] = $appointmentData['address'];
			$appointmentDataToSend['service_label'] = $appointmentData['service_label'];
			$appointmentDataToSend['profile_pic_url'] = $appointmentData['profile_pic_url'];
		} 
		return $appointmentDataToSend;
	}
	
	function generateAndSaveAppointmentCode($appointmentId, $serviceProviderId, $serviceId){
		$service_model = new service_model();
		$serviceCode = $service_model->getServiceCode($serviceId);
		$appointmentCode = $serviceCode.$serviceProviderId."-".$appointmentId;
		$appointment_model = new appointment_model();
		$appointmentCodeSaved = $appointment_model->insertAppointmentCode($appointmentId, $appointmentCode);
		return $appointmentCodeSaved;
	}
	
	function checkAndUpdateBookingStatus($appointmentId) {
		sleep(120);
		$appointment_model = new appointment_model();
		$pending = $appointment_model->isAppointmentStillPending($appointmentId);
		if ($pending) {
			$this->rejectAppointmentById($appointmentId, false);
		}
	}
	
	function getPendingAppointmentsForProvider_post() {
		$appointment_model = new appointment_model();
    	$pendingAppointments = $appointment_model->getPendingAppointmentsData($this->post('service_provider_id'));
    	if ($pendingAppointments) {
    		$this->response(array(
    			'message' => 'Pending appointments successfully viewed', 
    			'success' => '1',
    			'pending_appointments' => $pendingAppointments), 200);
    	} else {
    		$this->response(array(
    			'message' => 'No Pending appointments exist', 
    			'success' => '0',
    			'pending_appointments' => null), 200);
    	}
	}
}