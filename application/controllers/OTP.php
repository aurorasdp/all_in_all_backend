<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';


class OTP extends REST_Controller
{
	function sendsms_post() {
		if(!$this->post('phone')) {
			$this->response(array(
					'message' => 'Missing parameters',
					'success' => '0'), 400);
		}
		$otp = rand(1000, 9999);
	    $otp_prefix = ': ';
	    //Your message to send, Add URL encoding here.
	    $message = urlencode('Your verification code for All in All App is'. $otp_prefix. $otp);
	    $response_type = 'json';
	    //Define route 
	    $route = "4";
	    //Prepare you post parameters
	    $postData = array(
	        'authkey' => '107828A0dS46BNwLgM56ea8504',
	        'mobiles' => $this->post('phone'),
	        'message' => $message,
	        'sender' => 'ANHIVE',
	        'route' => $route,
	        'response' => $response_type
	    );
		//API URL
	    $url = "https://control.msg91.com/sendhttp.php";
		// init the resource
	    $ch = curl_init();
	    curl_setopt_array($ch, array(
	        CURLOPT_URL => $url,
	        CURLOPT_RETURNTRANSFER => true,
	        CURLOPT_POST => true,
	        CURLOPT_POSTFIELDS => $postData
	            //,CURLOPT_FOLLOWLOCATION => true
	    ));
	    //Ignore SSL certificate verification
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	    //get response
	    $output = curl_exec($ch);
	    //Print error if any
	    if (curl_errno($ch)) {
	    	$this->response(array(
	    			'message' => 'Error'. curl_error($ch),
	    			'success' => '0'), 400);
	        echo 'error:' . curl_error($ch);
	    }
	    curl_close($ch);
	    $this->response(array(
	    		'message' => 'Message was sent successfully',
	    		'OTP' => $otp,
	    		'success' => '1'), 200);
	}
}