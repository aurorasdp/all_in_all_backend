<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';
require (APPPATH.'/models/user_model.php');
//require (APPPATH.'/models/service_provider_model.php');
//require (APPPATH.'/models/appointment_model.php');

class User extends REST_Controller {
	
	function signup_post() {
		if(!$this->post('name') || !$this->post('country_code') || !$this->post('phone')
			|| !$this->post('email') || !$this->post('password') || !$this->post('address')) {
			$this->response(array('message' => 'Missing parameters',
								  'user_id' => null,
								  'success' => '0'), 200);
		}
		if (!$this->post('reg_id') || $this->post('reg_id') == "") {
        		$this->response(array('message' => 'Error missing registration id',
        						  'user_id' => null,
        						  'success' => '0'), 200);
        }
        $user_model = new user_model();
        if($user_model->alreadyExists($this->post('phone'))) {
        	$this->response(array('message' => 'User with this phone number already exists',
        						  'user_id' => null,
        						  'success' => '0'), 200);
        } else {
        	$userId = $user_model->insert($this->post('name'), $this->post('country_code'), $this->post('phone'), $this->post('email'), $this->post('password'), $this->post('address'), $this->post('reg_id'));
	        if ($userId == null) {
	        	$this->response(array('message' => 'Error while inserting to Database',
	        						  'user_id' => null,
	        					      'success' => '0'), 200);
	        } else {
		        if(!$this->post('profile_pic') || !$this->post('image_extension')) {
					$this->response(array('message' => 'User is registered successfully',
										  'user_id' => $userId,
										  'success' => '1'), 200);
				} else {
					$path = FCPATH ."uploads/users/".$userId.".".$this->post('image_extension');
					if ($user_model->insertUserProfilePic($userId, $path)) {
						file_put_contents($path,base64_decode($this->post('profile_pic')));
						$this->response(array('message' => 'User is registered successfully',
										      'user_id' => $userId,
										      'success' => '1'), 200);
					} else {
						$this->response(array('message' => 'Error while inserting to Database',
											  'user_id' => null,
											  'success' => '0'), 200);
					}
				}
	        }
        }
    }
    
	function login_post() {
		if(!$this->post('phone') || !$this->post('password')) {
			$this->response(array('message' => 'Missing parameters',
			                      'user_id' => null,
								  'success' => '0'), 200);
		}
        $user_model = new user_model();
		$country_code = null;
        $phone = $this->post('phone');
        if (stripos($this->post('phone'), '0091') !== false && stripos($this->post('phone'), '0091') == 0) {
   			$country_code = '0091';
   			$phone = substr($this->post('phone'), 4);
		} else if (stripos($this->post('phone'), '91') !== false &&  stripos($this->post('phone'), '91') == 0) {
			$country_code = '91';
			$phone = substr($this->post('phone'), 2);
		}
        $userId = $user_model->validLoginData($country_code, $phone, $this->post('password'));
        if (!$userId) {
        	$this->response(array('message' => 'Invalid phone number or password',
        						  'user_id' => null,
        	                      'success' => '0'), 200);
        } else {
        	if (!$this->post('reg_id') || $this->post('reg_id') == "") {
        		$this->response(array('message' => 'Error missing registration id',
        						  'user_id' => $userId,
        						  'success' => '0'), 200);
        	} else {
        		$user_model->updateRegistrationId($userId, $this->post('reg_id'));
        		$this->response(array('message' => 'User logged in successfully',
        						  'user_id' => $userId,
        						  'success' => '1'), 200);
        	}
        }
    }
    
   
    
	function signout_post() {
        $user_model = new user_model();
		$userId = $user_model->removeRegistrationId($this->post('user_id'));
	    $this->response(array('message' => 'User signed out successfully',
							      'user_id' => $userId,
							      'success' => '1'), 200);
    }
    
    
	function sendMailByNewPassword_post() {
		$user_model = new user_model();
		$country_code = null;
        $phone = $this->post('phone');
        if (stripos($this->post('phone'), '0091') !== false && stripos($this->post('phone'), '0091') == 0) {
   			$country_code = '0091';
   			$phone = substr($this->post('phone'), 4);
		} else if (stripos($this->post('phone'), '91') !== false &&  stripos($this->post('phone'), '91') == 0) {
			$country_code = '91';
			$phone = substr($this->post('phone'), 2);
		}
		$userData = $user_model->getUserData($phone);
		if ($userData) {
			if(filter_var($userData['email'], FILTER_VALIDATE_EMAIL)) {
			    //Email is valid
			    $passwordUpdated = $user_model->updateUserPassword($userData['id'], $this->post('encrypted_password'));
				$msg = "Hi ".$userData['name'].",\nYour new password is:".$this->post('unencrypted_password')." for your account with phone number:".$this->post('phone');
				$headers = "From: AllinAll <info@allinall.com> \r\n";
				$mailSent = mail($userData['email'], "Password Reset", $msg, $headers);
				if (!$mailSent) {
					$this->response(array('message' => 'Mail not sent',
							      'success' => '0'), 200);
				} else {
					$this->response(array('message' => 'Password reset and mail sent successfully',
							      'success' => '1'), 200);
				}
			} else {
				$this->response(array('message' => 'Invalid email',
							      'success' => '0'), 200);
			}
		} else {
			$this->response(array('message' => 'No user with this phone',
							      'success' => '0'), 200);
		}
		
    }
    
	function getUserEmail_post() {
		$user_model = new user_model();
		$country_code = null;
        $phone = $this->post('phone');
        if (stripos($this->post('phone'), '0091') !== false && stripos($this->post('phone'), '0091') == 0) {
   			$country_code = '0091';
   			$phone = substr($this->post('phone'), 4);
		} else if (stripos($this->post('phone'), '91') !== false &&  stripos($this->post('phone'), '91') == 0) {
			$country_code = '91';
			$phone = substr($this->post('phone'), 2);
		}
		$userEmail = $user_model->getUserEmail($phone);
		if ($userEmail) {
			$this->response(array('message' => 'User email retrieved successfully',
								  'email' => $userEmail,
							      'success' => '1'), 200);
		} else {
			$this->response(array('message' => 'No user with this phone',
								  'email' => null,
							      'success' => '0'), 200);
		}
		
    }
   
}
