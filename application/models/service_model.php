<?php defined('BASEPATH') OR exit('No direct script access allowed');


include_once 'system/core/Model.php';
class service_model extends CI_Model
{
	function __construct()
	{
		parent::__construct(); // construct the Model class
		$this->load->database();
	}
	
	function getServices() {
		$this->db->select('service.id, service.name, service.label');
		$this->db->from('service');
		$query = $this->db->get();
		if($query->num_rows()>0) {
			$result = $query->result_array();
			$i = 0;
			$therapyServices = array();
			foreach ($result as $row) {
				$therapyServices[$i]['service_id'] = $row['id'];
				$therapyServices[$i]['service_name'] = $row['name'];
				$therapyServices[$i]['service_label'] = $row['label'];
				$i = $i + 1;
			}
			return $therapyServices;
		} else {
			return false;
		} 
	}
	
	function getServiceLabels() {
		$this->db->select('service.id, service.label');
		$this->db->from('service');
		$query = $this->db->get();
		if($query->num_rows()>0) {
			$result = $query->result_array();
			$i = 0;
			$therapyServices = array();
			foreach ($result as $row) {
				$therapyServices[$i]['service_id'] = $row['id'];
				$therapyServices[$i]['service_label'] = $row['label'];
				$i = $i + 1;
			}
			return $therapyServices;
		} else {
			return false;
		} 
	}
	
	function getServiceProvidersList($serviceId) {
		$conditionsString = "service_data.service_id =".$serviceId." AND service_provider.balance > 0";
		$this->db->select('service_data.longitude, service_data.latitude, service_data.address, service_provider.id, service_provider.name, service_provider.bookings_count, service_provider.application_usage_count');
		$this->db->from('service_data');
		$this->db->join('service_provider', 'service_data.service_provider_id = service_provider.id');
		$this->db->where($conditionsString);
		$this->db->order_by("bookings_count", "asc");
		$this->db->order_by("application_usage_count", "desc");
		$query = $this->db->get();
		
		if($query->num_rows()>0) {
			$result = $query->result_array();
			$i = 0;
			$serviceProviders = array();
			foreach ($result as $row) {
				$serviceProviders[$i]['service_provider_id'] = $row['id'];
				$serviceProviders[$i]['service_provider_name'] = $row['name'];
				$serviceProviders[$i]['longitude'] = $row['longitude'];
				$serviceProviders[$i]['latitude'] = $row['latitude'];
				$serviceProviders[$i]['address'] = $row['address'];
				$serviceProviders[$i]['bookings_count'] = $row['bookings_count'];
				$serviceProviders[$i]['application_usage_count'] = $row['application_usage_count'];
				$i = $i + 1;
			}
			return $serviceProviders;
		} else {
			return false;
		}
	}
	
	function getServiceName($serviceId) {
		$conditionsString = "id =".$serviceId;
		$this->db->select('name');
		$this->db->from('service');
		$this->db->where($conditionsString);
		$query = $this->db->get();
		if($query->num_rows()>0) {
			$result = $query->result_array();
			return $result[0]['name'];
		} else {
			return false;
		}
	}
	
	function getServiceCode($serviceId) {
		$conditionsString = "id =".$serviceId;
		$this->db->select('code');
		$this->db->from('service');
		$this->db->where($conditionsString);
		$query = $this->db->get();
		if($query->num_rows()>0) {
			$result = $query->result_array();
			return $result[0]['code'];
		} else {
			return false;
		}
	}
	
}