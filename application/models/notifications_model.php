<?php defined('BASEPATH') OR exit('No direct script access allowed');


include_once 'system/core/Model.php';
class notifications_model extends CI_Model
{
	function __construct() {
		parent::__construct(); // construct the Model class
		$this->load->database();
	}
	
	function getUserRegID($userId) {
		$this->db->select('registration_id');
		$this->db->where('id',$userId);
		$query=$this->db->get('user');
		if ($query->num_rows()>0) {
			$result = $query->result_array();
			return $result[0]['registration_id'];
		}
		return null;
	}
	
	function getServiceProviderRegID($serviceProviderId) {
		$this->db->select('registration_id');
		$this->db->where('id',$serviceProviderId);
		$query=$this->db->get('service_provider');
		if ($query->num_rows()>0) {
			$result = $query->result_array();
			return $result[0]['registration_id'];
		}
		return null;
	}
}
