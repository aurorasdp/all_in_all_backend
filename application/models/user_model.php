<?php defined('BASEPATH') OR exit('No direct script access allowed');


include_once 'system/core/Model.php';
class user_model extends CI_Model
{
	function __construct() {
		parent::__construct(); // construct the Model class
		$this->load->database();
	}
	
	function insert($name, $countryCode, $phone, $email, $password, $address, $regId) {
		$post_data = array('name' => $name, 'country_code' => $countryCode, 'phone' => $phone, 'email' => $email,
						   'password' => $password, 'address' => $address, 'registration_id' => $regId);
		$this->db->insert('user', $post_data);
   		$insert_id = $this->db->insert_id();
  		return  $insert_id;
	}
	
	function insertUserProfilePic($userId, $profilePic) {
		$data = array('profile_pic_url' => $profilePic);
		$conditionsArray = array('id' => $userId);
		$this->db->where($conditionsArray);
		$this->db->update('user', $data);
		if ($this->db->affected_rows() == '1') {
			return true;
		} else {
			return false;
		}
	}
	
	function updateRegistrationId($userId, $regId) {
		$data = array('registration_id' => $regId);
		$conditionsArray = array('id' => $userId);
		$this->db->where($conditionsArray);
		$this->db->update('user', $data);
		if ($this->db->affected_rows() == '1') {
			return true;
		} else {
			return false;
		}
	}
	
	function removeRegistrationId($userId) {
		$data = array('registration_id' => '');
		$conditionsArray = array('id' => $userId);
		$this->db->where($conditionsArray);
		$this->db->update('user', $data);
		if ($this->db->affected_rows() == '1') {
			return true;
		} else {
			return false;
		}
	}
	
	function alreadyExists($phone) {
		$this->db->where('phone',$phone);
		$query=$this->db->get('user');
		if($query->num_rows() > 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	function validLoginData($country_code, $phone, $password) {
		if ($country_code != null) {
			$conditionsArray = "(country_code = '0091' OR country_code = '91' OR country_code = '') AND phone= '".$phone."' AND password = '".$password."'";
		} else {
			$conditionsArray = array('phone' => $phone, 'password' => $password);
		}
		$this->db->where($conditionsArray);
		$query=$this->db->get('user');
		if($query->num_rows() > 0) {
			$userProfileData = $query->result_array();
			return $userProfileData[0]['id'];
		}
		else {
			return false;
		}
	}
		
	
	/*
	function getUserProfileInformation($userId) {
		$this->db->select('name, age, email, phone, language, gender, address, blood_group, profile_pic_url');
		$this->db->from('user');
		$this->db->where('user.id', $userId);
		$query = $this->db->get();
		
		if($query->num_rows()>0) {
			$userProfileData = $query->result_array();
			return $userProfileData[0];
		} else {
			return false;
		}
	}
	
	
	function changeUserPassword($userId, $oldPassword, $newPassword) {
		$data = array('password' => $newPassword);
		$conditionsArray = array('id' => $userId, 'password' => $oldPassword);
		$this->db->where($conditionsArray);
		$this->db->update('user', $data);
		if ($this->db->affected_rows() == '1') {
			return true;
		} else {
			return false;
		}
	}
	
	function editUserProfileInformation($userId, $name, $age, $email, $phone, $language, $gender, $address) {
		$data = array('name' => $name, 'age' => $age, 'email' => $email, 'phone' => $phone, 'address' => $address, 'language' => $language, 'gender' => $gender);
		$this->db->where('id', $userId);
		$this->db->update('user', $data);
		if ($this->db->affected_rows() == '1') {
			return true;
		} else {
			return false;
		}
	}*/
	
	function getUserData($phone) {
		$this->db->select('id, name, email');
		$this->db->from('user');
		$this->db->where('user.phone', $phone);
		$query = $this->db->get();
		
		if($query->num_rows()>0) {
			$userProfileData = $query->result_array();
			return $userProfileData[0];
		} else {
			return false;
		}
	}
	
	function updateUserPassword($userId, $encryptedPassword) {
		$data = array('password' => $encryptedPassword);
		$this->db->where('id', $userId);
		$this->db->update('user', $data);
		if ($this->db->affected_rows() == '1') {
			return true;
		} else {
			return false;
		}
	}
	
	function getUserEmail($phone) {
		$this->db->select('email');
		$this->db->from('user');
		$this->db->where('user.phone', $phone);
		$query = $this->db->get();
		
		if($query->num_rows()>0) {
			$userProfileData = $query->result_array();
			return $userProfileData[0]['email'];
		} else {
			return false;
		}
	}
		
}
