<?php defined('BASEPATH') OR exit('No direct script access allowed');


include_once 'system/core/Model.php';
class service_data_model extends CI_Model
{
	function __construct()
	{
		parent::__construct(); // construct the Model class
		$this->load->database();
	}
	
	function insert($service_provider_id, $service_id, $address) {
		$post_data = array('service_provider_id' => $service_provider_id, 'service_id' => $service_id, 'address' => $address);
		$this->db->insert('service_data', $post_data);
   		$insert_id = $this->db->insert_id();
  		return  $insert_id;
	}

}