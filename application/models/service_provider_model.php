<?php defined('BASEPATH') OR exit('No direct script access allowed');


include_once 'system/core/Model.php';
class service_provider_model extends CI_Model
{
	function __construct()
	{
		parent::__construct(); // construct the Model class
		$this->load->database();
	}
	
	function insert($name, $countryCode, $phone, $email, $password, $regId)	{
		$post_data = array('name' => $name, 'country_code' => $countryCode, 'phone' => $phone, 'email' => $email, 'password' => $password, 'registration_id' => $regId);
		$this->db->insert('service_provider', $post_data);
   		$insert_id = $this->db->insert_id();
  		return  $insert_id;
	}
	
	function insertServiceProviderProfilePic($serviceProviderId, $profilePic) {
		$data = array('profile_pic_url' => $profilePic);
		$conditionsArray = array('id' => $serviceProviderId);
		$this->db->where($conditionsArray);
		$this->db->update('service_provider', $data);
		if ($this->db->affected_rows() == '1') {
			return true;
		} else {
			return false;
		}
	}
	
	function alreadyExists($phone) {
		$this->db->where('phone',$phone);
		$query=$this->db->get('service_provider');
		if($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	function validLoginData($country_code, $phone, $password) {
		if ($country_code != null) {
			$conditionsArray = "(country_code = '0091' OR country_code = '91' OR country_code = '') AND phone= '".$phone."' AND password = '".$password."'";
		} else {
			$conditionsArray = array('phone' => $phone, 'password' => $password);
		}
		$this->db->where($conditionsArray);
		$query=$this->db->get('service_provider');
		if($query->num_rows() > 0) {
			$serviceProviderData = $query->result_array();
			return $serviceProviderData[0]['id'];
		}
		else {
			return false;
		}
	}
	
	
	function getWalletData($service_provider_id) {
		$this->db->select('balance, scheme_type, phone');
		$this->db->from('service_provider');
		$this->db->where('id',$service_provider_id);
		$query = $this->db->get();
		if($query->num_rows()>0) {
			$result = $query->result_array();
			$walletData = array();
			$walletData['balance'] = $result[0]['balance'];
			$walletData['scheme_type'] = $result[0]['scheme_type'];
			$walletData['phone'] = $result[0]['phone'];
			return $walletData;
		} else {
			return false;
		}
	}

	function removeRegistrationId($serviceProviderId) {
		$data = array('registration_id' => '');
		$conditionsArray = array('id' => $serviceProviderId);
		$this->db->where($conditionsArray);
		$this->db->update('service_provider', $data);
		if ($this->db->affected_rows() == '1') {
			return true;
		} else {
			return false;
		}
	}
	
	function getServiceProviderData($phone) {
		$this->db->select('id, name, email');
		$this->db->from('service_provider');
		$this->db->where('service_provider.phone', $phone);
		$query = $this->db->get();
		
		if($query->num_rows()>0) {
			$serviceProviderProfileData = $query->result_array();
			return $serviceProviderProfileData[0];
		} else {
			return false;
		}
	}
	
	function updateServiceProviderPassword($serviceProviderId, $encryptedPassword) {
		$data = array('password' => $encryptedPassword);
		$this->db->where('id', $serviceProviderId);
		$this->db->update('service_provider', $data);
		if ($this->db->affected_rows() == '1') {
			return true;
		} else {
			return false;
		}
	}
	
	function incrementApplicationUsageCount($serviceProviderId) {
		$this->db->set('application_usage_count', 'application_usage_count + 1', FALSE);
   		$this->db->where('id', $serviceProviderId);
   		$this->db->update('service_provider');
		if ($this->db->affected_rows() == '1') {
			return true;
		} else {
			return false;
		}
	}
	
	function updateRegistrationId($serviceProviderId, $regId) {
		$data = array('registration_id' => $regId);
		$conditionsArray = array('id' => $serviceProviderId);
		$this->db->where($conditionsArray);
		$this->db->update('service_provider', $data);
		if ($this->db->affected_rows() == '1') {
			return true;
		} else {
			return false;
		}
	}
}