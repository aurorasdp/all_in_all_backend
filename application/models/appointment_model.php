<?php defined('BASEPATH') OR exit('No direct script access allowed');


include_once 'system/core/Model.php';
class appointment_model extends CI_Model
{
	function __construct()
	{
		parent::__construct(); // construct the Model class
		$this->load->database();
	}
	
		
	function getConfirmedAppointmentsDataByServiceProviderId($serviceProviderId, $currentDate, $currentTime) {
		$conditionsString = "service_provider_id =".$serviceProviderId." AND status ='Confirmed' AND ((date > '".$currentDate."') OR (date = '".$currentDate."' AND time > '".$currentTime."'))";
		$this->db->select('appointment.id, appointment.user_id, user.name as user_name, user.profile_pic_url, appointment.date, appointment.time, service.name as service_name, user.country_code, user.phone, user.address');
		$this->db->from('appointment');
		$this->db->join('user', 'user.id = appointment.user_id');
		$this->db->join('service', 'service.id = appointment.service_id');
		$this->db->where($conditionsString);
		$this->db->order_by("date", "desc");
		$this->db->order_by("time", "desc");
		$query = $this->db->get();
		
		if($query->num_rows()>0) {
			$result = $query->result_array();
			$i = 0;
			$appointmentsData = array();
			foreach ($result as $row) {
				$appointmentsData[$i]['booking_id'] = $row['id'];
				$appointmentsData[$i]['user_id'] = $row['user_id'];
				$appointmentsData[$i]['user_name'] = $row['user_name'];
				$appointmentsData[$i]['appointment_date'] = $row['date'];
				$appointmentsData[$i]['appointment_time'] = $row['time'];
				$appointmentsData[$i]['service_name'] = $row['service_name'];
				$appointmentsData[$i]['user_country_code'] = $row['country_code'];
				$appointmentsData[$i]['user_pic'] = $row['profile_pic_url'];
				$appointmentsData[$i]['user_phone'] = $row['phone'];
				$appointmentsData[$i]['user_address'] = $row['address'];
				$i = $i + 1;
			}
			return $appointmentsData;
		} else {
			return false;
		}
	}
	
	function getAppointmentsDataByUserId($userId, $currentDate, $currentTime) {
		$conditionsString = "user_id =".$userId." AND (status = 'Pending' OR status = 'Confirmed') AND ((date > '".$currentDate."') OR (date = '".$currentDate."' AND time > '".$currentTime."'))";
		$this->db->select('appointment.id, service_provider.name as provider_name, service_provider.profile_pic_url, appointment.date,
						 appointment.time, service.label as service_label, appointment.status, service_data.address, appointment.appointment_code');
		$this->db->from('appointment');
		$this->db->join('service_provider', 'service_provider.id = appointment.service_provider_id');
		$this->db->join('service_data', 'appointment.service_provider_id = service_data.service_provider_id AND appointment.service_id = service_data.service_id');
		$this->db->join('service', 'service.id = appointment.service_id');
		$this->db->where($conditionsString);
		$this->db->order_by("date", "desc");
		$this->db->order_by("time", "desc");
		$query = $this->db->get();
		
		if($query->num_rows()>0) {
			$result = $query->result_array();
			$i = 0;
			$appointmentsData = array();
			foreach ($result as $row) {
				$appointmentsData[$i]['appointment_id'] = $row['id'];
				$appointmentsData[$i]['service_provider_name'] = $row['provider_name'];
				$appointmentsData[$i]['service_provider_pic'] = $row['profile_pic_url'];
				$appointmentsData[$i]['appointment_date'] = $row['date'];
				$appointmentsData[$i]['appointment_time'] = $row['time'];
				$appointmentsData[$i]['service_label'] = $row['service_label'];
				$appointmentsData[$i]['status'] = $row['status'];
				$appointmentsData[$i]['address'] = $row['address'];
				$appointmentsData[$i]['appointment_code'] = $row['appointment_code'];
				$i = $i + 1;
			}
			return $appointmentsData;
		} else {
			return false;
		}
	}
	
	function cancelAppointmentById($appointmentId) {
		$appointmentCancelled = false;
		$data = array('status' => "Cancelled");
		$this->db->where('id', $appointmentId);
		$this->db->update('appointment', $data);
		if ($this->db->affected_rows() == '1') {
    		$appointmentCancelled = true;
		}
		return $appointmentCancelled;
	}
	
	function confirmAppointmentById($appointmentId) {
		$appointmentConfirmed = false;
		$data = array('status' => "Confirmed");
		$this->db->where('id', $appointmentId);
		$this->db->update('appointment', $data);
		if ($this->db->affected_rows() == '1') {
    		$appointmentConfirmed = true;
		}
		return $appointmentConfirmed;
	}
	
	function completeAppointmentById($appointmentId) {
		$appointmentCompleted = false;
		$data = array('status' => "Completed");
		$this->db->where('id', $appointmentId);
		$this->db->update('appointment', $data);
		if ($this->db->affected_rows() == '1') {
    		$appointmentCompleted = true;
		}
		return $appointmentCompleted;
	}
	
	function reviewAppointmentById($appointmentId, $review) {
		$appointmentReviewed = false;
		$data = array('review' => $review);
		$this->db->where('id', $appointmentId);
		$this->db->update('appointment', $data);
		if ($this->db->affected_rows() == '1') {
    		$appointmentReviewed = true;
		}
		return $appointmentReviewed;
	}
	
	
	function insert($user_id, $service_provider_id, $date, $time, $service_id, $userLongitude, $userLatitude, $carType, $hourlyOrOutStation, $rejectedServiceProviders, $bookNow) {
		$this->db->trans_start();
		if (!$carType && !$hourlyOrOutStation) {
			if (!$rejectedServiceProviders) {
				$post_data = array('user_id' => $user_id, 'service_provider_id' => $service_provider_id, 'date' => $date, 'time' => $time,
							 'service_id' => $service_id, 'status' => 'Pending', 'user_longitude' => $userLongitude, 'user_latitude' => $userLatitude, 'book_now' => $bookNow);
			} else {
				$post_data = array('user_id' => $user_id, 'service_provider_id' => $service_provider_id, 'date' => $date, 'time' => $time,
							 'service_id' => $service_id, 'status' => 'Pending', 'rejected_service_providers' => $rejectedServiceProviders,
							 'user_longitude' => $userLongitude, 'user_latitude' => $userLatitude, 'book_now' => $bookNow);
			}
		} else {
			if (!$rejectedServiceProviders) {
				$post_data = array('user_id' => $user_id, 'service_provider_id' => $service_provider_id, 'date' => $date, 'time' => $time,
							 'service_id' => $service_id, 'status' => 'Pending', 'car_type' => $carType, 'hourly_or_outstation' => $hourlyOrOutStation,
							 'user_longitude' => $userLongitude, 'user_latitude' => $userLatitude, 'book_now' => $bookNow);
			} else {
				$post_data = array('user_id' => $user_id, 'service_provider_id' => $service_provider_id, 'date' => $date, 'time' => $time,
							 'service_id' => $service_id, 'status' => 'Pending', 'car_type' => $carType, 'hourly_or_outstation' => $hourlyOrOutStation,
							 'rejected_service_providers' => $rejectedServiceProviders, 'user_longitude' => $userLongitude, 'user_latitude' => $userLatitude, 'book_now' => $bookNow);
			}
		}
		$this->db->insert('appointment', $post_data);
   		$insert_id = $this->db->insert_id();
   		
   		$this->db->set('bookings_count', 'bookings_count + 1', FALSE);
   		$this->db->where('id', $service_provider_id);
   		$this->db->update('service_provider');
   		$this->db->trans_complete();
  		return  $insert_id;
	}
	
	function getInsertedAppointmentsData($appointmentId) {
		$conditionsArray = array('appointment.id' => $appointmentId );
		$this->db->select('appointment.id as appointment_id, appointment.appointment_code, service_provider.name as service_provider_name, appointment.date,
						 appointment.time, appointment.status, service_data.address, service.name as service_name, service.label as service_label, appointment.service_provider_id,
						 appointment.car_type, appointment.hourly_or_outstation, appointment.user_longitude, appointment.user_latitude, service_provider.profile_pic_url');
		$this->db->from('appointment');
		$this->db->join('service_provider', 'service_provider.id = appointment.service_provider_id');
		$this->db->join('service_data', 'appointment.service_provider_id = service_data.service_provider_id AND appointment.service_id = service_data.service_id');
		$this->db->join('service', 'service.id = appointment.service_id');
		$this->db->where($conditionsArray);
		$query = $this->db->get();
		
		if($query->num_rows()>0) {
			$result = $query->result_array();
			$i = 0;
			$appointmentsData = array();
			foreach ($result as $row) {
				$appointmentsData[$i]['appointment_id'] = $row['appointment_id'];
				$appointmentsData[$i]['appointment_code'] = $row['appointment_code'];
				$appointmentsData[$i]['service_provider_id'] = $row['service_provider_id'];
				$appointmentsData[$i]['service_provider_name'] = $row['service_provider_name'];
				$appointmentsData[$i]['service_name'] = $row['service_name'];
				$appointmentsData[$i]['appointment_date'] = $row['date'];
				$appointmentsData[$i]['appointment_time'] = $row['time'];
				$appointmentsData[$i]['status'] = $row['status'];
				$appointmentsData[$i]['address'] = $row['address'];
				$appointmentsData[$i]['car_type'] = $row['car_type'];
				$appointmentsData[$i]['hourly_or_outstation'] = $row['hourly_or_outstation'];
				$appointmentsData[$i]['user_longitude'] = $row['user_longitude'];
				$appointmentsData[$i]['user_latitude'] = $row['user_latitude'];
				$appointmentsData[$i]['service_label'] = $row['service_label'];
				$appointmentsData[$i]['profile_pic_url'] = $row['profile_pic_url'];
				$i = $i + 1;
			}
			return $appointmentsData[0];
		} else {
			return false;
		}
	}
	
	function getBusyAppointmentsForServiceProvider($serviceProviderId, $date) {
		$query = $this->db->query("SELECT date, time FROM appointment WHERE service_provider_id =".$serviceProviderId." AND (status='Pending' OR status='Confirmed') AND date >= '".$date."'");
		if($query->num_rows()>0) {
			$result = $query->result_array();
			return $result;
		} else {
			return false;
		}
	}
				
	function getHistoryByUserId($userId, $dateBeforeOneHour, $timeBeforeOneHour) {
		$conditionsString = "user_id =".$userId." AND ((status ='Completed') OR (status ='Confirmed' AND ((date < '".$dateBeforeOneHour."') OR (date = '".$dateBeforeOneHour."' AND time < '".$timeBeforeOneHour."'))))";
		$this->db->select('appointment.id, service_provider.name as provider_name, appointment.date, service_provider.profile_pic_url, appointment.time, service.label as service_label, service_data.address, appointment.appointment_code, appointment.review');
		$this->db->from('appointment');
		$this->db->join('service_provider', 'service_provider.id = appointment.service_provider_id');
		$this->db->join('service', 'service.id = appointment.service_id');
		$this->db->join('service_data', 'appointment.service_provider_id = service_data.service_provider_id AND appointment.service_id = service_data.service_id');
		$this->db->where($conditionsString);
		$this->db->order_by("date", "desc");
		$this->db->order_by("time", "desc");
		$query = $this->db->get();
		
		if($query->num_rows()>0) {
			$result = $query->result_array();
			$i = 0;
			$appointmentsData = array();
			foreach ($result as $row) {
				$appointmentsData[$i]['appointment_id'] = $row['id'];
				$appointmentsData[$i]['service_provider_name'] = $row['provider_name'];
				$appointmentsData[$i]['service_provider_pic'] = $row['profile_pic_url'];
				$appointmentsData[$i]['appointment_date'] = $row['date'];
				$appointmentsData[$i]['appointment_time'] = $row['time'];
				$appointmentsData[$i]['service_label'] = $row['service_label'];
				$appointmentsData[$i]['address'] = $row['address'];
				$appointmentsData[$i]['appointment_code'] = $row['appointment_code'];
				$appointmentsData[$i]['review'] = $row['review'];
				$i = $i + 1;
			}
			return $appointmentsData;
		} else {
			return false;
		}
	}
	
	function getAppointmentsDataByAppointmentId($appointmentId) {
		$conditionsString = "appointment.id =".$appointmentId;
		$this->db->select('appointment.id, service_provider.name as provider_name, appointment.date,
						 appointment.time, service.name as service_name, appointment.user_id, service_provider.phone as service_provider_phone,
						 appointment.car_type, appointment.hourly_or_outstation, appointment.service_id, appointment.service_provider_id,
						 appointment.rejected_service_providers, appointment.user_longitude, appointment.user_latitude, appointment.book_now');
		$this->db->from('appointment');
		$this->db->join('service_provider', 'service_provider.id = appointment.service_provider_id');
		$this->db->join('service', 'service.id = appointment.service_id');
		$this->db->where($conditionsString);
		$query = $this->db->get();
		
		if($query->num_rows()>0) {
			$result = $query->result_array();
			$appointmentsData = array();
			$appointmentsData[0]['appointment_id'] = $result[0]['id'];
			$appointmentsData[0]['service_provider_name'] = $result[0]['provider_name'];
			$appointmentsData[0]['appointment_date'] = $result[0]['date'];
			$appointmentsData[0]['appointment_time'] = $result[0]['time'];
			$appointmentsData[0]['service'] = $result[0]['service_name'];
			$appointmentsData[0]['user_id'] = $result[0]['user_id'];
			$appointmentsData[0]['service_provider_phone'] = $result[0]['service_provider_phone'];
			$appointmentsData[0]['car_type'] = $result[0]['car_type'];
			$appointmentsData[0]['hourly_or_outstation'] = $result[0]['hourly_or_outstation'];
			$appointmentsData[0]['service_id'] = $result[0]['service_id'];
			$appointmentsData[0]['service_provider_id'] = $result[0]['service_provider_id'];
			$appointmentsData[0]['rejected_service_providers'] = $result[0]['rejected_service_providers'];
			$appointmentsData[0]['user_longitude'] = $result[0]['user_longitude'];
			$appointmentsData[0]['user_latitude'] = $result[0]['user_latitude'];
			$appointmentsData[0]['book_now'] = $result[0]['book_now'];
			return $appointmentsData[0];
		} else {
			return false;
		}
	}
	
	function getPendingAppointmentsNumberForUser($userId, $currentDate, $currentTime) {
		$conditionsString = "user_id =".$userId." AND status ='Pending' AND ((date > '".$currentDate."') OR (date = '".$currentDate."' AND time > '".$currentTime."'))";
		$this->db->select('count(*)');
		$this->db->from('appointment');
		$this->db->where($conditionsString);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result[0]['count(*)'];
	}
	
	function countConfirmedAppointmentsWithinTimes($serviceProviderId, $dateBeforeOneHour, $timeBeforeOneHour, $dateAfterOneHour, $timeAfterOneHour, $exceptAppointmentId) {
		if ($dateBeforeOneHour == $dateAfterOneHour) {
			$conditionsString = "service_provider_id =".$serviceProviderId." AND id !=".$exceptAppointmentId." AND status ='Confirmed' AND ((date = '".$dateBeforeOneHour."') AND (time >= '".$timeBeforeOneHour."') AND (time <= '".$timeAfterOneHour."'))";
		} else {
			$conditionsString = "service_provider_id =".$serviceProviderId." AND id !=".$exceptAppointmentId." AND status ='Confirmed' AND (((date = '".$dateBeforeOneHour."') AND (time >= '".$timeBeforeOneHour."')) OR ((date = '".$dateAfterOneHour."') AND (time <= '".$timeAfterOneHour."')))";
		}
		$this->db->select('count(*)');
		$this->db->from('appointment');
		$this->db->where($conditionsString);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result[0]['count(*)'];
	}
	
	function saveRejectedServiceProvider($appointmentId, $rejectedServiceProviders) {
		$data = array('rejected_service_providers' => $rejectedServiceProviders);
   		$this->db->where('id', $appointmentId);
   		$this->db->update('appointment', $data);
		if ($this->db->affected_rows() == '1') {
			return true;
		} else {
			return false;
		}
	}
	
	function getPendingAppointments($serviceProviderId) {
		$conditionsArray = array('appointment.service_provider_id' => $serviceProviderId, 'appointment.status' => 'Pending');
		$this->db->select('service_provider.name as service_provider_name, appointment.date,
						 appointment.time, service.name as service_name, appointment.id as appointment_id,
						 appointment.car_type, appointment.hourly_or_outstation');
		$this->db->from('appointment');
		$this->db->join('service_provider', 'service_provider.id = appointment.service_provider_id');
		$this->db->join('service', 'service.id = appointment.service_id');
		$this->db->where($conditionsArray);
		$query = $this->db->get();
		
		if($query->num_rows()>0) {
			$result = $query->result_array();
			$i = 0;
			$appointmentsData = array();
			foreach ($result as $row) {
				$appointmentsData[$i]['service_provider_name'] = $row['service_provider_name'];
				$appointmentsData[$i]['service_name'] = $row['service_name'];
				$appointmentsData[$i]['appointment_date'] = $row['date'];
				$appointmentsData[$i]['appointment_time'] = $row['time'];
				$appointmentsData[$i]['car_type'] = $row['car_type'];
				$appointmentsData[$i]['hourly_or_outstation'] = $row['hourly_or_outstation'];
				$appointmentsData[$i]['appointment_id'] = $row['appointment_id'];
				$i = $i + 1;
			}
			return $appointmentsData;
		} else {
			return false;
		}
	}
	
	function countBusyAppointments($serviceProviderId, $dateBeforeOneHour, $timeBeforeOneHour, $dateAfterOneHour, $timeAfterOneHour) {
		if ($dateBeforeOneHour == $dateAfterOneHour) {
			$conditionsString = "service_provider_id =".$serviceProviderId." AND status ='Confirmed' AND ((date = '".$dateBeforeOneHour."') AND (time >= '".$timeBeforeOneHour."') AND (time <= '".$timeAfterOneHour."'))";
		} else {
			$conditionsString = "service_provider_id =".$serviceProviderId." AND status ='Confirmed' AND (((date = '".$dateBeforeOneHour."') AND (time >= '".$timeBeforeOneHour."')) OR ((date = '".$dateAfterOneHour."') AND (time <= '".$timeAfterOneHour."')))";
		}
		$this->db->select('count(*)');
		$this->db->from('appointment');
		$this->db->where($conditionsString);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result[0]['count(*)'];
	}
	
	function insertAppointmentCode($appointmentId, $appointmentCode) {
		$data = array('appointment_code' => $appointmentCode);
		$conditionsArray = array('id' => $appointmentId);
		$this->db->where($conditionsArray);
		$this->db->update('appointment', $data);
		if ($this->db->affected_rows() == '1') {
			return true;
		} else {
			return false;
		}
	}
	
	function isAppointmentStillPending($appointmentId) {
		$conditionsString = "id =".$appointmentId." AND status = 'Pending'";
		$this->db->select('count(*)');
		$this->db->from('appointment');
		$this->db->where($conditionsString);
		$query = $this->db->get();
		$result = $query->result_array();
		if($result[0]['count(*)']>0) {
			return true;
		} else {
			return false;
		}
	}
	
	function isAppointmentCancelled($appointmentId) {
		$conditionsString = "id =".$appointmentId." AND status = 'Cancelled'";
		$this->db->select('count(*)');
		$this->db->from('appointment');
		$this->db->where($conditionsString);
		$query = $this->db->get();
		$result = $query->result_array();
		if($result[0]['count(*)']>0) {
			return true;
		} else {
			return false;
		}
	}
	
	function getPendingAppointmentsData($serviceProviderId) {
		$conditionsArray = array('appointment.service_provider_id' => $serviceProviderId, 'appointment.status' => 'Pending');
		$this->db->select('appointment.date, appointment.time, service.name as service_name, appointment.id as appointment_id,
						 appointment.car_type, appointment.hourly_or_outstation');
		$this->db->from('appointment');
		$this->db->join('service_provider', 'service_provider.id = appointment.service_provider_id');
		$this->db->join('service', 'service.id = appointment.service_id');
		$this->db->where($conditionsArray);
		$query = $this->db->get();
		
		if($query->num_rows()>0) {
			$result = $query->result_array();
			$i = 0;
			$appointmentsData = array();
			foreach ($result as $row) {
				$appointmentsData[$i]['service_name'] = $row['service_name'];
				$appointmentsData[$i]['appointment_date'] = $row['date'];
				$appointmentsData[$i]['appointment_time'] = $row['time'];
				$appointmentsData[$i]['car_type'] = $row['car_type'];
				$appointmentsData[$i]['hourly_or_outstation'] = $row['hourly_or_outstation'];
				$appointmentsData[$i]['appointment_id'] = $row['appointment_id'];
				$i = $i + 1;
			}
			return $appointmentsData;
		} else {
			return false;
		}
	}
}