<?php defined('BASEPATH') OR exit('No direct script access allowed');


include_once 'system/core/Model.php';
class doctor_working_hours_model extends CI_Model
{
	function __construct()
	{
		parent::__construct(); // construct the Model class
		$this->load->database();
	}
	
		
	function getWorkingHoursForServiceProvider($serviceProviderId, $serviceId) {
		$conditionsArray = array('service_provider_id' => $serviceProviderId, 'service_id' => $serviceId);
		$this->db->where($conditionsArray);
		$query=$this->db->get('service_provider_working_hours');
		if($query->num_rows()>0) {
			$result = $query->result_array();
			return $result[0];
		} else {
			return false;
		} 
	}
}